#!/bin/bash

echo -e "\033[1;32mConceitos de substituição de variáveis: \e[0m"
echo " "
echo "O Bash executa a substituição de variáveis antes que o comando seja realmente executado. O Linux Bash Shell procura todos os sinais '$' antes de executar o comando e substitui-os pelo valor da variável"
echo "Exemplo: "
echo "    nome=joao"
echo "    \$nome"
sleep 1
echo " "
echo -e "\033[1;33mConceitos de substituição de shell: \e[0m"
echo ""
echo "Você consegue guardar um comando executado dentro de uma variável, utilizando $ + ()"
echo "    data=\$(comando)"
sleep 1
echo " "
echo -e "\033[1;34mConceitos de substituição aritmética: \e[0m"
echo " "
echo "Uma expressão aritmética entre parênteses duplos precedida por um sinal de dólar (” $ (()) “) é substituída pelo valor da expressão aritmética entre parênteses duplos."
echo "    valor=\$((valor))"



