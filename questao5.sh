#!/bin/bash


echo "Criando /tmp/DATA: "
x=$(date +%y-%m-%d-%H)
mkdir /tmp/$x
echo "Copiando todos os arquivos para o diretorio criado: "
sleep 1
cp ./* /tmp/$x 2> /dev/null
echo "Compactando o diretorio"
tar -czf $x.tar.gz /tmp/$x 2> /dev/null
echo "Movendo o diretorio"
mv /tmp/$x/$x.tar.gz ~
echo "Excluido diretorio criado"
rm -rf /tmp/$x
echo "Tudo certo :)"
